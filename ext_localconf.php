<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtCountry',
            'Hiveextcountrycheckboxlist',
            [
                'Country' => 'checkboxList'
            ],
            // non-cacheable actions
            [
                'Country' => 'checkboxList'
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtCountry',
            'Hiveextcountryselectlist',
            [
                'Country' => 'selectList'
            ],
            // non-cacheable actions
            [
                'Country' => 'selectList'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hiveextcountrycheckboxlist {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_country') . 'Resources/Public/Icons/user_plugin_hiveextcountrycheckboxlist.svg
                        title = LLL:EXT:hive_ext_country/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_country_domain_model_hiveextcountrycheckboxlist
                        description = LLL:EXT:hive_ext_country/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_country_domain_model_hiveextcountrycheckboxlist.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextcountry_hiveextcountrycheckboxlist
                        }
                    }
                    hiveextcountryselectlist {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_country') . 'Resources/Public/Icons/user_plugin_hiveextcountryselectlist.svg
                        title = LLL:EXT:hive_ext_country/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_country_domain_model_hiveextcountryselectlist
                        description = LLL:EXT:hive_ext_country/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_country_domain_model_hiveextcountryselectlist.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextcountry_hiveextcountryselectlist
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
