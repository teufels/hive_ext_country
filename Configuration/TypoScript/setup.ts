
plugin.tx_hiveextcountry_hiveextcountrycheckboxlist {
    view {
        templateRootPaths.0 = EXT:hive_ext_country/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveextcountry_hiveextcountrycheckboxlist.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_country/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveextcountry_hiveextcountrycheckboxlist.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_country/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveextcountry_hiveextcountrycheckboxlist.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveextcountry_hiveextcountrycheckboxlist.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hiveextcountry_hiveextcountryselectlist {
    view {
        templateRootPaths.0 = EXT:hive_ext_country/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveextcountry_hiveextcountryselectlist.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_country/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveextcountry_hiveextcountryselectlist.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_country/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveextcountry_hiveextcountryselectlist.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveextcountry_hiveextcountryselectlist.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hiveextcountry._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-ext-country table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-ext-country table th {
        font-weight:bold;
    }

    .tx-hive-ext-country table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hiveextcountry {

    # Standard PID aus den Konstanten
        persistence {
            storagePid = {$plugin.tx_hiveextcountry.persistence.storagePid}
        }

        model {
            HIVE\HiveExtCountry\Domain\Model\Country {
                persistence {
                    storagePid = {$plugin.tx_hiveextcountry.model.HIVE\HiveExtCountry\Domain\Model\Country.persistence.storagePid}
                }
            }
            HIVE\HiveExtCountry\Domain\Model\Abstract {
                persistence {
                    storagePid = {$plugin.tx_hiveextcountry.model.HIVE\HiveExtCountry\Domain\Model\City.persistence.storagePid}
                }
            }
        }

        classes {
            HIVE\HiveExtCountry\Domain\Model\Country.newRecordStoragePid = {$plugin.tx_hiveextcountry.model.HIVE\HiveExtCountry\Domain\Model\Country.persistence.storagePid}
            HIVE\HiveExtCountry\Domain\Model\Abstract.newRecordStoragePid = {$plugin.tx_hiveextcountry.model.HIVE\HiveExtCountry\Domain\Model\Abstract.persistence.storagePid}
        }

    }
}